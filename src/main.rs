use bevy::core::FixedTimestep;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::channel;
use std::sync::Mutex;
use std::sync::Arc;
use std::time::Duration;
use bevy::app::PluginGroupBuilder;
use bevy::prelude::*;
use bevy_egui::{egui, EguiContext};
use bevy_egui::egui::plot::{Line, Plot, Value, Values};
use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use rtrb::{RingBuffer};

#[derive(Debug)]
struct Counter(f32);

pub struct MyPlugins;
impl PluginGroup for MyPlugins {
    fn build(&mut self, group: &mut PluginGroupBuilder) {
        group.add(bevy::log::LogPlugin::default());
        group.add(bevy::core::CorePlugin::default());
        group.add(bevy::transform::TransformPlugin::default());
        group.add(bevy::diagnostic::DiagnosticsPlugin::default());
        group.add(bevy::input::InputPlugin::default());
        group.add(bevy::window::WindowPlugin::default());
        group.add(bevy::asset::AssetPlugin::default());
        group.add(bevy::scene::ScenePlugin::default());
        group.add(bevy::render::RenderPlugin::default());
        group.add(bevy::sprite::SpritePlugin::default());
        group.add(bevy::pbr::PbrPlugin::default());
        group.add(bevy::ui::UiPlugin::default());
        group.add(bevy::text::TextPlugin::default());
        group.add(bevy::gilrs::GilrsPlugin::default());
        group.add(bevy::gltf::GltfPlugin::default());
        group.add(bevy::winit::WinitPlugin::default());
        group.add(bevy::wgpu::WgpuPlugin::default());
        group.add(bevy_egui::EguiPlugin);
        group.add(LogDiagnosticsPlugin::default());
        group.add(FrameTimeDiagnosticsPlugin::default());
    }
}

struct Queue {
    array: Vec<Value>,
    oldest_element: usize,
    capacity: usize
}

impl Queue {
    fn new(capacity: usize) -> Queue {
        let mut array = Vec::with_capacity(capacity);
        array.resize(capacity, Value::new(0.0, 0.0));
        Queue {
            array,
            oldest_element: capacity,
            capacity
        }
    }

    fn insert(&mut self, value: Value) {
        let index = (self.oldest_element + self.capacity) % self.capacity;
        self.array[index] = value;
        self.oldest_element += 1;

    }
}

struct FrameTimer(Timer);

fn main() {
    let (mut producer, mut consumer) = RingBuffer::new(1024);

    let (sender, receiver) = channel::<Value>();
    // Audio caputure thread
    std::thread::spawn(move || {
        let mut x: f64 = 0.0;
        loop {
            if let Err(e) = producer.push(Value::new(x, x.sin())) {
                println!("Error: audio processing thread too slow and not picking up audio samples {}", e);
            };
            std::thread::sleep(Duration::from_secs_f64(1.0 / 48_000.0));
            x += 0.0001;
        }
    });
    
    // Audio process thread
    std::thread::spawn(move || {
        loop {
            if consumer.is_empty() {
              std::thread::sleep(Duration::from_secs_f64(1.0 / 96_000.0));
              continue
            }
            match consumer.read_chunk(consumer.slots()) {
                Ok(chunk) => {
                    chunk.into_iter().for_each(|c| {
                        if let Err(e) = sender.send(c) {
                            println!("Error while sending to renderer {}", e);
                        }
                    });
                },
                Err(e) => println!("Error {}", e)
            }
        }
    });
    App::build()
        .add_plugins(MyPlugins)
        .insert_resource(Counter(0.0))
        .insert_resource(Queue::new(1000))
        .insert_non_send_resource(receiver)
        .add_system(ui_example.system())
        .add_stage_after(CoreStage::Update, "fixed_update", SystemStage::parallel()
            .with_run_criteria(FixedTimestep::step(1.0 / 120.0))
            .with_system(insert_amp.system())
)
        .run();
}

fn insert_amp(mut queue: ResMut<Queue>, receiver: NonSend<Receiver<Value>>) {
    loop {
        if let Ok(value) = receiver.try_recv() {
            queue.insert(value.clone());
        } else {
            break;
        }
    }
}

fn ui_example(egui_ctx: ResMut<EguiContext>, counter: ResMut<Counter>, 
    queue: ResMut<Queue>) {
    //println!("Now: {:?}", timer.0.tick(time.delta()));
    let mut counter = counter;

    //let test = consumer;
     
    egui::Area::new("test")
        .show(egui_ctx.ctx(), |ui| {

        let line = Line::new(Values::from_values(queue.array.clone()));
        ui.add(
            Plot::new("testplot")
                .line(line)
                .view_aspect(2.0)
                .allow_drag(false)
                .show_x(false)
                .show_y(false)
        );

    });
    egui::Area::new("test")
        .show(egui_ctx.ctx(), |ui| {
            if ui.button("-").clicked() {
                counter.0 -= 1.0;
            }
            ui.label(counter.0.to_string());
            if ui.button("+").clicked() {
                counter.0 += 1.0;
            }
        });
}
